/*
 * fifo.h
 *
 *  Created on: 01.11.2016
 *  Author: Kirill Snezhko
 */

#ifndef FIFO_H_
#define FIFO_H_

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define RESULT_OK                   ((uint8_t)0)
#define RESULT_ERROR_NO_MEMORY      ((uint8_t)1)
#define RESULT_ERROR_EMPTY_QUEUE    ((uint8_t)2)
#define RESULT_ERROR_QUEUE_IS_BUSY  ((uint8_t)3)

#define LOCK_QUEUE  do {isBusy = 1;} while (0)
#define UNLOCK_QUEUE  do {isBusy = 0;} while (0)

#define MAX_QUEUE_SIZE ((uint16_t)2048)

static volatile uint16_t currentQueueSize = 0;
static volatile uint16_t messageCounter = 0;
static volatile uint8_t isBusy = 0;

typedef struct Message {
  uint16_t length;
  struct Message *next;
  uint8_t body[];
} Message;

static Message *head = 0;
static Message *tail = 0;

static uint8_t *messageToReturn;

uint8_t enqueue(uint8_t*, uint16_t);
uint8_t unqueue(uint8_t**, uint16_t);
uint8_t clearQueue(void);
uint16_t getQueueLength(void);


#endif /* FIFO_H_ */
