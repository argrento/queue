# Использование

    #include "fifo.h"
    void main() {
      uint8_t test1[3] = {0, 1, 2};
      uint8_t test2[3] = {3, 4, 5};
      uint8_t test3[3] = {6, 7, 8};
      enqueue(test1, 3);
      enqueue(test2, 3);
      enqueue(test3, 3);
      uint8_t *test4;
      uint16_t length = 0;
      unqueue(&test4, length);
      unqueue(&test4, length);
      unqueue(&test4, length);
      enqueue(test1, 3);
      enqueue(test2, 3);
      unqueue(&test4, length);
      unqueue(&test4, length);
    }

# Параметры
Запуск и тестирование осуществляется с помощью System Workbench for STM32 (SW4STM32), версии:

* C/C++ Debugging Tools for MCU Version: 1.11.0.201610101240
* C/C++ Embedded Development Tools for MCU Version: 1.11.0.201610101240
* Linker Script Editor Version: 1.11.0.201610101240
* GNU Tools ARM Embedded (for Windows 32bits) Version: 1.11.0.201610101338
* OpenOCD (for Windows 32bits) Version: 1.11.0.201610101338