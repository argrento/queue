/*
 * fifo.c
 *
 *  Created on: 01.11.2016
 *  Author: Kirill Snezhko
 */

#include "fifo.h"

uint8_t enqueue(uint8_t *message, uint16_t length) {
  if (isBusy == 1) {
    return RESULT_ERROR_QUEUE_IS_BUSY;
  }

  LOCK_QUEUE;

  if ((currentQueueSize + length) > MAX_QUEUE_SIZE) {
    return RESULT_ERROR_NO_MEMORY;
  }

  currentQueueSize += length;
  messageCounter++;
  Message *m = malloc(sizeof(struct Message) + length);
  m->length = length;
  memcpy(m->body, message, length);
  m->next = 0;

  if (head == 0) {
    head = m;
    tail = head;
  }

  if (tail == 0) {
    tail = m;
  } else {
    tail->next = m;
    tail = m;
  }

  UNLOCK_QUEUE;

  return RESULT_OK;
}

uint8_t unqueue(uint8_t **message, uint16_t length) {
  if (isBusy == 1) {
    return RESULT_ERROR_QUEUE_IS_BUSY;
  }

  LOCK_QUEUE;

  if (head == 0){
    return RESULT_ERROR_EMPTY_QUEUE;
  }

  *message = (uint8_t*)malloc(length);
  memcpy(*message, head->body, head->length);
  length = head->length;

  if (head->next == 0) {
    free(head);
    head = 0;
  } else {
    Message *temp = head->next;
    temp->next = head->next->next;
    free(head);
    head = temp;
  }

  currentQueueSize -= length;
  messageCounter--;

  UNLOCK_QUEUE;

  return RESULT_OK;
}

uint8_t clearQueue(void) {

  if (isBusy == 1) {
    return RESULT_ERROR_QUEUE_IS_BUSY;
  }

  LOCK_QUEUE;

  Message *temp = head;
  while (messageCounter > 0) {
    temp = head;
    free(head);
    head = temp->next;
    messageCounter--;
  }

  UNLOCK_QUEUE;

  return RESULT_OK;
}

uint16_t getQueueLength(void) {
  return messageCounter;
}
